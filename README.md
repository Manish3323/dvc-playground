# Dvc Playground
In this setup, i have already set dvc to track some data files (xml type).

[Open in Gitpod](https://gitpod.io/#/https://gitlab.com/Manish3323/dvc-playground)

## Pre-req

Add azure creds( this will not be pushed to git)

    > dvc remote modify --local remoteazure account_name 'manishbackstagetechdocs'
    > dvc remote modify --local remoteazure account_key '.......' 

Account key can be found in 'access keys' section of 'manishbackstagetechdocs' storage account.
run again dvc pull to verify it works

    > dvc pull 

now check /data/ folder , few xml files will be downloaded in this workspace which you can work with.

## Add nhl_teams.xml to the dvc playground



    > dvc get-url 'https://www.kaggle.com/datasets/sveneschlbeck/nhl-data-for-all-teams-seasons-2008-2021?resource=download&select=nhl_game.csv' data/nhl_game.csv

run dvc add csv file from azure to the workspace

    > dvc add nhl_game.csv

Now you can play with this file & create some output.csv file

once, you are done with your workflow

    > dvc add <your-output.file>    # for tracking in dvc
    > dvc push <your-output.file>   # push to azure
    > git commit -m 'message'      # to create snapshot of your work
    > git push
